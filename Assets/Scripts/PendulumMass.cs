using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PendulumMass : MonoBehaviour {
    [SerializeField] private GameObject minMaxSlider;
    [SerializeField] private GameObject valueTextObject;
    [SerializeField] private GameObject pendulum;

    private Rigidbody pendulumRb;
    private Slider slider;
    private Text valueText;

    // Get components and initialize slider
    void Awake() {
        slider = minMaxSlider.GetComponent<Slider>();
        valueText = valueTextObject.GetComponent<Text>();
        pendulumRb = pendulum.GetComponent<Rigidbody>();
        slider.value = pendulumRb.mass * 1000;
        valueText.text = slider.value + " g";
    }

    private void Update() {
        valueText.text = slider.value + " g";
    }

    public void UpdateMass() {
        float newMass = slider.value / 1000f;
        pendulumRb.mass = newMass;
    }
}