using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PendulumLength : MonoBehaviour {
    [SerializeField] private GameObject minMaxSlider;
    [SerializeField] private GameObject valueTextObject;
    [SerializeField] private GameObject pendulumBall;
    [SerializeField] private GameObject pendulumRod;
    private Slider slider;
    private Text valueText;


    void Awake() {
        slider = minMaxSlider.GetComponent<Slider>();
        valueText = valueTextObject.GetComponent<Text>();
        // *100 for cm conversion, *2 because for some reason 0.5 y-scale for the rod is equal to a 1x1x1 cube
        slider.value = pendulumRod.transform.localScale.y * 200f; 
        valueText.text = slider.value + " cm";
    }

    private void Update() {
        valueText.text = slider.value + " cm";
    }

    public void UpdateLength() {
        // store previous length and ball position to compensate length change
        Vector3 previousSize = pendulumRod.transform.localScale;
        Vector3 ballPreviousPosition = pendulumBall.transform.localPosition;
        float newLength = slider.value / 200f;
        pendulumRod.transform.localScale = new Vector3(previousSize.x, newLength, previousSize.z);
        
        // Move ball to compensate length change
        float ballYDiff = 2f*(newLength - previousSize.y);
        pendulumBall.transform.localPosition = new Vector3(ballPreviousPosition.x, ballPreviousPosition.y - ballYDiff,
            ballPreviousPosition.z);
    }
}