using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class AmplitudeDisplay : MonoBehaviour {
    [SerializeField] private Transform pendulumBall;
    [SerializeField] private Text amplitudeText;
    [SerializeField] private Transform pendulumAttachment;
    [SerializeField] private XRGrabInteractable pendulum;

    private bool grabbed = false;

    private void Awake() {
        pendulum.selectExited.AddListener(OnRelease);
        pendulum.selectEntered.AddListener(OnGrab);
    }
    
    private void OnGrab(SelectEnterEventArgs arg0) {
        grabbed = true;
    }

    private void OnRelease(SelectExitEventArgs arg0) {
        grabbed = false;
    }


    void Update() {
        // once the user releases the pendulum, the amplitude display keeps the last value
        if (grabbed) {
            Vector3 currentPosition = pendulumBall.position;
            float amplitude = Mathf.Abs(currentPosition.x - pendulumAttachment.position.x);
            amplitudeText.text = "Amplitude: " + Mathf.Round(amplitude * 1000f) * 0.1f + " cm";
        }

    }
}