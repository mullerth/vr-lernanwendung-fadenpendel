using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    private static bool stopwatchActive = false;
    private static float startTime = 0f;
    private static float endTime = 0f;
    private static string times;

    [SerializeField] private Text boardText;
    
    // Activate or deactivate stopwatch, on stop: write to board
    public void ToggleStopwatch() {
        stopwatchActive = !stopwatchActive;
        if (stopwatchActive) {
            startTime = Time.time;
        } else {
            endTime = Time.time;
            float elapsedTime = endTime - startTime;
            elapsedTime = (float)Math.Round(elapsedTime, 2);
            times += elapsedTime + "\t\t";
            boardText.text = times;
            startTime = 0f;
            endTime = 0f;
            
        }
    }

    // when stopwatch is activated, write measurement to board and reset stopwatch
    public void SplitTime() {
        // Allows time measurements in quick succession
        if (stopwatchActive) {
            endTime = Time.time;
            float elapsedTime = endTime - startTime;
            elapsedTime = (float)Math.Round(elapsedTime, 2);
            times += elapsedTime + "\t\t";
            boardText.text = times;
            startTime = Time.time;
            endTime = 0f;
        }
    }
}