using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CelestialBodies : MonoBehaviour {
    
    [SerializeField] private GameObject mercury;
    [SerializeField] private GameObject venus;
    [SerializeField] private GameObject earth;
    [SerializeField] private GameObject mars;
    [SerializeField] private GameObject jupiter;
    [SerializeField] private GameObject saturn;
    [SerializeField] private GameObject uranus;
    [SerializeField] private GameObject neptune;
    [SerializeField] private GameObject moon;

    private List<GameObject> celestialBodies = new();

    private const float MercuryGravity = -3.70f;
    private const float VenusGravity = -8.87f;
    private const float EarthGravity = -9.81f;
    private const float MarsGravity = -3.71f;
    private const float JupiterGravity = -24.79f;
    private const float SaturnGravity = -10.44f;
    private const float UranusGravity = -9.01f;
    private const float NeptuneGravity = -11.15f;
    private const float MoonGravity = -1.62f;

    // Default gravity is 9.81 m/s^2, so earth will be visible when entering the application
    void Awake() {
        celestialBodies.AddRange(new List<GameObject>() {
            mercury, venus, earth, mars, jupiter, saturn, uranus, neptune, moon
        });
        EnableCelestialBody(earth);
    }
    
    private void DisableAll() {
        foreach (GameObject celestialBody in celestialBodies) {
            celestialBody.SetActive(false);
        }
    }

    private void EnableCelestialBody(GameObject celestialBody) {
        DisableAll();
        celestialBody.SetActive(true);
    }

    // show corresponding celestial body depending on the gravity. When no gravity matches for an implemented
    // celestial body, none will be shown. 
    public void SetActiveCelestialBody() {
        switch (Physics.gravity.y) {
            default:
                DisableAll();
                break;
            case MercuryGravity:
                EnableCelestialBody(mercury);
                break;
            case VenusGravity:
                EnableCelestialBody(venus);
                break;
            case EarthGravity:
                EnableCelestialBody(earth);
                break;
            case MarsGravity:
                EnableCelestialBody(mars);
                break;
            case JupiterGravity:
                EnableCelestialBody(jupiter);
                break;
            case SaturnGravity:
                EnableCelestialBody(saturn);
                break;
            case UranusGravity:
                EnableCelestialBody(uranus);
                break;
            case NeptuneGravity:
                EnableCelestialBody(neptune);
                break;
            case MoonGravity:
                EnableCelestialBody(moon);
                break;
        }
    }
}