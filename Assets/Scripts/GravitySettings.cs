using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class GravitySettings : MonoBehaviour {
    [SerializeField] private GameObject minMaxSlider;
    [SerializeField] private GameObject valueTextObject;
    private Slider slider;
    private Text valueText;
    
    void Awake() {
        slider = minMaxSlider.GetComponent<Slider>();
        // default gravity is earth's gravity (9.81 m/s^2)
        SetSlider(981f);
        valueText = valueTextObject.GetComponent<Text>();
    }
    
    void Update() {
        valueText.text = slider.value/100 + " m/s²";
    }

    // set gravity and update slider text
    public void UpdateGravity() {
        Physics.gravity = new Vector3(0, -slider.value/100, 0);
        valueText.text = slider.value/100 + " m/s²";
    }

    // used for presets
    public void SetSlider(float newValue) {
        slider.value = newValue;
    }
}